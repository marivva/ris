package MainFolder;

import org.apache.commons.cli.ParseException;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class MainClass {
    private static final QName CHANGESET = new QName("changeset");
    private static final QName USER = new QName("user");
    private static final QName UID = new QName("uid");

    private static final String DEFAULT_INPUT_FILE_NAME = "RU-NVS.osm.bz2";
    private static final String DEFAULT_OUTPUT_XML_FILE_NAME = "output.xml";
    private static final String DEFAULT_STATISTIC_OUTPUT_FILE_NAME = "statistic.xml";
    private static final Long DEFAULT_NUMBER_OF_SYMBOLS_TO_READ = Long.MAX_VALUE;
    private static final Integer DEFAULT_BUFFER_LENGTH = 1000;
    private static final Integer CHAR_SIZE = 32;

    private static final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private static final Logger logger = LogManager.getLogger(MainClass.class);

    private static Statistic statistic;

    private static String inputFileName;
    private static URL archiveURL;
    private static String archivePath;

    private static CommandLine commandLine;

    private static BZip2CompressorInputStream inputStream;
    private static FileOutputStream outputStream;

    private static Long numberOfSymbolsToRead;
    private static Boolean calcStat;

    public static void main(String[] args) {
        //init and get cli options
        initHelpers(args);

        try {
            convertBz2ToXML();
            if (calcStat) {
                long startTime = System.currentTimeMillis();
                    statistic = calcStatistic();
                long endTime = System.currentTimeMillis();

                logger.info("Time taken for calc statistic: " + (endTime - startTime));
                logger.info("Speed: " +  1000.0f * inputStream.getBytesRead() / (endTime - startTime) / 1024 / 1024);

                saveStatisticToFile();
                logger.info("Statistic saved");
            }
        } catch (Exception exception){
            logger.error(exception.getMessage(), exception);
        }
    }

    private static void initHelpers(String[] args) {
        try {
            commandLine = createCommandLine(args);
            inputStream = getInputStream();
            outputStream = getOutputStream();
        } catch (ParseException | IOException exception){
            logger.error(exception.getMessage());
        }
        numberOfSymbolsToRead = getNumberOfSymbolsToRead();
        calcStat = getCalcStat();
        logger.info("Reading from: " + inputFileName +
                        ", writing convert .bz2 to .xml into: " + DEFAULT_OUTPUT_XML_FILE_NAME +
                        ", with calculating statistic: " + calcStat.toString() +
                (calcStat ? ", writing into: " + DEFAULT_STATISTIC_OUTPUT_FILE_NAME : ""));
    }

    private static BZip2CompressorInputStream getInputStream() throws IOException{
        inputFileName = commandLine.getOptionValue("i", DEFAULT_INPUT_FILE_NAME);
        archiveURL = MainClass.class.getClassLoader().getResource(inputFileName);
        archivePath = archiveURL.getPath();

        FileInputStream fileInputStream = new FileInputStream(archivePath);

        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream, CHAR_SIZE * DEFAULT_BUFFER_LENGTH);
        return new BZip2CompressorInputStream(bufferedInputStream);
    }

    private static FileOutputStream getOutputStream() throws IOException {
        File file = new File(commandLine.getOptionValue("o", DEFAULT_OUTPUT_XML_FILE_NAME));
        return new FileOutputStream(file);
    }

    private static Long getNumberOfSymbolsToRead(){
        return Long.parseLong(commandLine.getOptionValue("n", "" + DEFAULT_NUMBER_OF_SYMBOLS_TO_READ));
    }

    private static Boolean getCalcStat(){
        return commandLine.hasOption('s');
    }

    private static CommandLine createCommandLine(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("i", "input", true, "input file path");
        options.addOption("o", "output", true, "output file path");
        options.addOption("s", "statistic", false, "print statistic");
        options.addOption("n", "number", true, "number of symbols to read");
        DefaultParser commandLineParser = new DefaultParser();
        return commandLineParser.parse(options, args);
    }

    private static void convertBz2ToXML() throws IOException {
        IOUtils.copyRange(inputStream, numberOfSymbolsToRead, outputStream);
        logger.info("Copying success");
    }

    private static Statistic calcStatistic() throws IOException, XMLStreamException {
        Map<String, Integer> usersChanges = new HashMap<>();
        Map<String, Integer> nodesIds = new HashMap<>();

        XMLEventReader reader = xmlInputFactory.createXMLEventReader(
                new BZip2CompressorInputStream(new BufferedInputStream(new FileInputStream(archivePath), CHAR_SIZE * DEFAULT_BUFFER_LENGTH)));

        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();

            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();

                if (startElement.getName().getLocalPart().equals("node")) {
                    String user = startElement.getAttributeByName(USER).getValue();
                    String uid = startElement.getAttributeByName(UID).getValue();
                    Attribute changesetAttribute = startElement.getAttributeByName(CHANGESET);

                    if (changesetAttribute != null) {
                        usersChanges.merge(user, 1, Integer::sum);
                    }

                    nodesIds.merge(uid, 1, Integer::sum);
                }
            }
        }
        logger.info("Statistic calculated successfully");
        return new Statistic(usersChanges, nodesIds);
    }

    private static void saveStatisticToFile() throws XMLStreamException, TransformerException, IOException {
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        XMLStreamWriter xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream);

        statistic.usersChanges = statistic.usersChanges.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new
                ));
        statistic.nodesIds = statistic.nodesIds.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new
                ));

        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement("statistic");

        xmlStreamWriter.writeStartElement("users_changes");
        for (Map.Entry<String, Integer> entry : statistic.usersChanges.entrySet()){
            xmlStreamWriter.writeStartElement("entry");
            xmlStreamWriter.writeAttribute("user", entry.getKey());
            xmlStreamWriter.writeAttribute("count", entry.getValue().toString());
            xmlStreamWriter.writeEndElement();
        }
        xmlStreamWriter.writeEndElement();

        xmlStreamWriter.writeStartElement("nodes_IDs");
        for (Map.Entry<String, Integer> entry : statistic.nodesIds.entrySet()){
            xmlStreamWriter.writeStartElement("entry");
            xmlStreamWriter.writeAttribute("user", entry.getKey());
            xmlStreamWriter.writeAttribute("count", entry.getValue().toString());
            xmlStreamWriter.writeEndElement();
        }
        xmlStreamWriter.writeEndElement();

        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndDocument();

        xmlStreamWriter.flush();
        xmlStreamWriter.close();

        String xml = outputStream.toString();
        String prettyPrintXML = formatXML(xml);
        Files.write(Paths.get(DEFAULT_STATISTIC_OUTPUT_FILE_NAME), prettyPrintXML.getBytes("UTF-8"));
    }

    private static String formatXML(String xml) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        StreamSource source = new StreamSource(new StringReader(xml));
        StringWriter output = new StringWriter();
        transformer.transform(source, new StreamResult(output));

        return output.toString();
    }
}
