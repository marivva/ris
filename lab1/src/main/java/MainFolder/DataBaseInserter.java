package MainFolder;

//import generated.Node;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openstreetmap.osm._0.Node;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DataBaseInserter {
    private static final QName CHANGESET = new QName("changeset");
    private static final QName USER = new QName("user");
    private static final QName UID = new QName("uid");

    private static final String DEFAULT_INPUT_FILE_NAME = "RU-NVS.osm.bz2";
    private static final String DEFAULT_OUTPUT_XML_FILE_NAME = "output.xml";
    private static final String DEFAULT_STATISTIC_OUTPUT_FILE_NAME = "statistic.xml";
    private static final Long DEFAULT_NUMBER_OF_SYMBOLS_TO_READ = Long.MAX_VALUE;
    private static final Integer DEFAULT_BUFFER_LENGTH = 1000;
    private static final Integer CHAR_SIZE = 32;

    private static final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private static final Logger logger = LogManager.getLogger(MainClass.class);

    private static CommandLine commandLine;

    private static String inputFileName;
    private static String outputFileName;
    private static String JdbcURL;
    private static String userName;
    private static String userPassword;
    private static String tableName;
    private static String methodName;
    private static Long numberOfSymbolsToRead;
    private static Boolean calcStat;

    private static BZip2CompressorInputStream inputStream;
    private static FileOutputStream outputStream;

    public static void main(String[] args) {
        //init and get cli options
        initCLI(args);

        Map <String, DataLoader> loaders = new HashMap<>();
        loaders.put("node insert", new NodeStmtImport());
        loaders.put("node prepared", new NodePreparedStmtImport(false));
        loaders.put("node batch", new NodePreparedStmtImport(true));
        loaders.put("node_json insert", new NodeJSONStmtImport());
        loaders.put("node_json prepared", new NodeJSONPreparedStmtImport(false));
        loaders.put("node_json batch", new NodeJSONPreparedStmtImport(true));

        getCLIOptions();

        try {
            inputStream = getInputStream();
            Connection connection = DriverManager.getConnection(JdbcURL, userName, userPassword);
            connection.setAutoCommit(false);
            if (outputFileName != null) {
                outputStream = new FileOutputStream(outputFileName);
                convertBz2ToXML();
            }
            PartialUnmarshaller nodeReader = new PartialUnmarshaller<>(inputStream, Node.class);
            DataLoader loader = loaders.get(tableName + ' ' + methodName);
            loader.cleanup(connection);
            connection.commit();
            long startTime = System.currentTimeMillis();
            int count = loader.loadData(nodeReader, connection, () -> inputStream.getBytesRead() > numberOfSymbolsToRead);
            connection.commit();
            long endTime = System.currentTimeMillis();

            logger.info("Time taken: " + (endTime - startTime));
            logger.info("Speed: " +  1000.0f * inputStream.getBytesRead() / (endTime - startTime) / 1024 / 1024 +
                    "; Node count: " + count +
                    "; Node speed: " + 1000.0f * count / (endTime - startTime));
        } catch (IOException | SQLException | JAXBException | XMLStreamException exception){
            logger.error(exception);
        }
    }

    private static void initCLI(String[] args) {
        try {
            commandLine = createCommandLine(args);
        } catch (ParseException exception){
            logger.error(exception.getMessage());
        }
        logger.info("CLI init");
    }

    private static CommandLine createCommandLine(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("i", "input", true, "input file path");
        options.addOption("o", "output", true, "output file path");
        options.addOption("s", "statistic", false, "print statistic");
        options.addOption("n", "number", true, "number of symbols to read");
        options.addOption("d", "database", true, "Database");
        options.addOption("t", "table", true, "node | node_json | node_ctype");
        options.addOption("m", "method", true, "insert | prepared statement | batch");
        options.addOption("u", "user", true, "JDBC username");
        options.addOption("p", "password", true, "JDBC password");
        DefaultParser commandLineParser = new DefaultParser();
        return commandLineParser.parse(options, args);
    }

    private static void getCLIOptions(){
        inputFileName = commandLine.getOptionValue('i', DEFAULT_INPUT_FILE_NAME);
        outputFileName = commandLine.getOptionValue('o');
        JdbcURL = commandLine.getOptionValue('d');
        userName = commandLine.getOptionValue('u');
        userPassword = commandLine.getOptionValue('p');
        tableName = commandLine.getOptionValue('t');
        methodName = commandLine.getOptionValue('m');
        calcStat = commandLine.hasOption('s');
        numberOfSymbolsToRead = Long.parseLong(commandLine.getOptionValue('n', "" + DEFAULT_NUMBER_OF_SYMBOLS_TO_READ));
        logger.info("Reading from: " + inputFileName + "; unpack: " + outputFileName + "; length: " + numberOfSymbolsToRead);
        logger.info("Inserting into: " + JdbcURL);
    }

    private static BZip2CompressorInputStream getInputStream() throws IOException{
//        URL archiveURL = MainClass.class.getClassLoader().getResource();
//        String archivePath = archiveURL.getPath();

        FileInputStream fileInputStream = new FileInputStream(inputFileName);

        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream, CHAR_SIZE * DEFAULT_BUFFER_LENGTH);
        return new BZip2CompressorInputStream(bufferedInputStream);
    }

    private static void convertBz2ToXML() throws IOException {
        IOUtils.copyRange(inputStream, numberOfSymbolsToRead, outputStream);
        logger.info("Copying success");
    }
}
